Introduction To AWS
===

This is a sample application to test [AWS](https://aws.amazon.com/) and [Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/) for [Play Framework](https://www.playframework.com/) application.


You need to [enable](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html) BitBucket Pipeline and [configure for AWS](https://confluence.atlassian.com/bitbucket/deploy-to-amazon-aws-875304040.html) .

![Bitbucket Pipeline Settings](introduction-to-aws.png "Bitbucket Pipeline Settings")


Don't forget the change [aws.config](aws.config) if you have different region for AWS (Default is *eu-center-1* ).
